<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('include/sidebar'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <?php $this->load->view('include/topbar'); ?>
                <!-- Top Bar End -->

                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">

                                    <h4>PIC INFORMATION</h4>
                                    <hr>
                                    <p><br></p>
                                    <form action="#" method="post" accept-charset="utf-8">
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Username SSO</label>
                                            <div class="col-10">
                                                <input type="text" class="form-control" placeholder="Username SSO" id="username">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Full Name</label>
                                            <div class="col-10">
                                                <input type="text" class="form-control" placeholder="Full Name" id="fullname">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">e-Mail</label>
                                            <div class="col-10">
                                                <input type="email" class="form-control" placeholder="e-Mail" id="email">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">No. HP</label>
                                            <div class="col-10">
                                                <input type="text" class="form-control" placeholder="No. HP" id="hp">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Jabatan</label>
                                            <div class="col-10">
                                                <input type="text" class="form-control" placeholder="Jabatan" id="jabatan">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Lokasi Kerja</label>
                                            <div class="col-10">
                                                <select class="selectpicker" data-live-search="true" data-style="btn-custom" title="Select option..." id="lokasi">
                                                    <?php foreach ($unit as $key): ?>
                                                        <option value="<?php echo $key->id_lokasi ?>"><?php echo $key->unit ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                        <button type="button" class="btn btn-primary waves-effect waves-light" id="add">Save</button>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php $this->load->view('include/footer'); ?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?php echo base_url('assets') ?>/plugins/sweet-alert/sweetalert2.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>

        <script>
            $(".select2").select2({placeholder: "Select for unit"});

            $('#add').click(function() {
                username = $('#username').val();
                fullname = $('#fullname').val();
                email = $('#email').val();
                hp = $('#hp').val();
                jabatan = $('#jabatan').val();
                lokasi = $('#lokasi').val();

                $.ajax({
                    url: '<?php echo base_url('pic/add_process') ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {'username': username, 'fullname': fullname, 'email': email, 'hp': hp, 'jabatan': jabatan, 'lokasi': lokasi},
                })
                .done(function() {
                    swal({
                        title: 'Good job!',
                        text: 'A record have been added!',
                        type: 'success',
                        timer: 3000,
                        confirmButtonClass: 'btn btn-confirm'
                    }).then(function() {
                        window.location.replace('<?php echo base_url('pic') ?>');
                    });
                })
                .fail(function() {
                    swal({
                        title: 'Error',
                        text: "Adding record failed!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                });
            })

        </script>

    </body>
</html>