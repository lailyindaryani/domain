<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('username') == false) {
			redirect('auth');
		}
		$this->load->model('m_crud');
	}

	public function index()
	{
		$data['admin'] = $this->m_crud->get_table('admin')->result();
		$this->load->view('admin/admin_home', $data);
	}

	public function add()
	{
		$fullname = $this->input->post('fullname');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$data = array(
			'full_name' => $fullname,
			'username_sso' => $username,
			'password' => md5($password),
			'email' => $email
		);

		$add_id = $this->m_crud->add($data, 'admin');

		echo json_encode($add_id);
	}

	public function edit()
	{
		$username = $this->input->post('username');
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$data = array(
			'full_name' => $fullname,
			'password' => md5($password),
			'email' => $email
		);

		$where = array('username_sso' => $username);

		$edit_id = $this->m_crud->edit($data, $where, 'admin');

		echo json_encode($edit_id);
	}

	public function delete()
	{
		$id = $this->input->post('id');

		$where = array('username_sso' => $id);

		$delete = $this->m_crud->delete($where, 'admin');

		echo json_encode($delete);
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */