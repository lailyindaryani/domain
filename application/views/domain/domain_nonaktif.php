<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('include/sidebar'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <?php $this->load->view('include/topbar'); ?>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <!-- contract file modal content -->
                        <div id="file-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="contractModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">File Contract</h4>
                                    </div>
                                        <div class="row">
                                            <div class="col-12 text-sm-center">
                                                <canvas id="the-canvas"></canvas>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Close</button>
                                        </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title">List Domains</h4>
                                    <br>

                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-12 text-sm-center form-inline">
                                                <label class="form-inline mr-3">
                                                    Show:
                                                    <select id="demo-show-entries" class="form-control form-control-sm ml-1 mr-1">
                                                        <option value="5">5</option>
                                                        <option value="10">10</option>
                                                        <option value="15">15</option>
                                                        <option value="20">20</option>
                                                    </select>
                                                </label>
                                                <div class="form-group mr-2">
                                                    <select id="demo-foo-filter-status" class="custom-select">
                                                        <option value="">Show all</option>
                                                        <option value="active">Active</option>
                                                        <option value="suspended">Suspended</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <input id="demo-foo-search" type="text" placeholder="Search" class="form-control" autocomplete="on">
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <table id="table-domain" class="table m-b-0 table-bordered toggle-arrow-tiny" data-page-size="5">
                                        <thead>
                                            <tr>
                                                <th data-toggle="true"> Domain </th>
                                                <th> Kategori </th>
                                                <th> Jenis Lokasi </th>
                                                <th> Expired Date </th>
                                                <th> Nama Admin </th>
                                                <th data-hide="all"> e-Mail </th>
                                                <th data-hide="all"> No. HP </th>
                                                <th data-hide="all"> Jabatan </th>
                                                <th data-hide="all"> Lokasi Kerja </th>
                                                <th data-hide="all"> User domain </th>
                                                <th data-hide="all"> IP Address </th>
                                                <th> Created Date </th>
                                                <th data-hide="all"> Sign Date </th>
                                                <th> Kontrak </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($domain as $key): ?>
                                            <tr>
                                                <td><?php echo $key->nama_domain ?></td>
                                                <td><?php echo $key->type ?></td>
                                                <td><?php echo $key->jenis ?></td>
                                                <td>
                                                    <?php if ($key->expired_date == '0000-00-00'): ?>
                                                        None
                                                    <?php else: ?>
                                                        <?php
                                                        echo date("j F Y", strtotime($key->expired_date));

                                                        echo "<br>";

                                                        $now = time();
                                                        $your_date = strtotime($key->expired_date);
                                                        $datediff = $your_date - $now;
                                                        $selisih = round($datediff / (60 * 60 * 24));
                                                        if ($selisih < 0) {
                                                            echo "<p class='text-danger'>( " . $selisih . " )</p>";
                                                        }
                                                        ?>          
                                                    <?php endif ?>
                                                </td>
                                                <td>
                                                    <?php if (is_null($key->username_sso)): ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-info btn-sm set_pic btn-rounded" title="Set PIC for this domain" data-toggle="modal" data-target="#pic-modal" data-id="<?php echo $key->nama_domain ?>" id='pic_button'> <i class="fi-head"></i> </button>
                                                    <?php else: ?>
                                                        <?php echo $key->nama_admin ?>
                                                    <?php endif ?>
                                                </td>
                                                <td><?php echo $key->email ?></td>
                                                <td><?php echo $key->no_hp ?></td>
                                                <td><?php echo $key->jabatan ?></td>
                                                <td><?php echo $key->unit ?></td>
                                                <td><?php echo $key->username_sso ?></td>
                                                <td><?php echo $key->ip_address ?></td>
                                                <td><?php echo $key->created_date ?></td>
                                                <td><?php echo $key->sign_date ?></td>
                                                <td>
                                                    <?php if (null === $key->file_kontrak): ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-info btn-sm view_contract" title="Upload contract file for this domain" data-toggle="modal" data-target="#upload-modal" data-id="<?php echo $key->nama_domain ?>" id='upload_button'> <i class="fi-upload"></i> </button>
                                                    <?php else: ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-light view_contract" title="View contract for this domain" data-filename="<?php echo $key->file_kontrak ?>" id='file_button'> <i class="fi-paper-clip"></i> </button>
                                                    <?php endif ?>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-icon waves-effect btn-success btn-rounded view_contract" title="Activate this domain" onclick="location.href='<?php echo base_url('domains/activation/') . $key->nama_domain ?>'"> <i class="fa fa-history"></i> </button>
                                                    <button type="button" class="btn btn-icon waves-effect btn-danger btn-rounded view_contract btn_delete" title="Delete this domain" data-id="<?php echo $key->nama_domain ?>"> <i class="fa fa-trash-o"></i> </button>
                                                </td>
                                            </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                        <tfoot>
                                            <tr class="active">
                                                <td colspan="15">
                                                    <div class="text-right">
                                                        <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>                                    

                                </div>

                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php $this->load->view('include/footer'); ?>

            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <script src="<?php echo base_url('assets') ?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>

        <!--FooTable-->
        <script src="<?php echo base_url('assets') ?>/plugins/footable/js/footable.all.min.js"></script>

        <!-- Tooltipster js -->
        <script src="<?php echo base_url('assets') ?>/plugins/tooltipster/tooltipster.bundle.min.js"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?php echo base_url('assets') ?>/plugins/sweet-alert/sweetalert2.min.js"></script>

        <!-- Select2 -->
        <script src="<?php echo base_url('assets') ?>/plugins/select2/js/select2.min.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>

        <script>
            $(".select2").select2();

            $(".btn_delete").click(function() {
                id = $(this).data("id");
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    $.ajax({
                        url: '<?php echo base_url('domains/perm_delete') ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {'id': id},
                    })
                    .done(function() {
                        swal({
                            title: 'Deleted !',
                            text: "Your record has been permanently deleted.",
                            type: 'success',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        }).then(function() {
                            location.reload();
                        });
                    })
                    .fail(function(res) {
                        swal({
                            title: 'Error',
                            text: "Deleting record failed!",
                            type: 'error',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        });
                        console.log(res);
                    });
                    
                });
            });

            var domain = $('#table-domain');
            domain.footable();

            $('#demo-show-entries').change(function (e) {
                e.preventDefault();
                var pageSize = $(this).val();
                domain.data('page-size', pageSize);
                domain.trigger('footable_filter');
            });

            domain.footable().on('footable_filtering', function (e) {
                var selected = $('#demo-foo-filter-status').find(':selected').val();
                e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                e.clear = !e.filter;
            });

            // Filter status
            $('#demo-foo-filter-status').change(function (e) {
                e.preventDefault();
                domain.trigger('footable_filter', {filter: $(this).val()});
            });

            // Search input
            $('#demo-foo-search').on('input', function (e) {
                e.preventDefault();
                domain.trigger('footable_filter', {filter: $(this).val()});
            });

            $('.view_contract').tooltipster();
            $('.set_pic').tooltipster();

            $('#upload_button').click(function() {
                $("#file_id").val($(this).data("id"))
            })

            $('#file_button').click(function () {
                file_name = $(this).data("filename");
                var url = 'http://localhost/domain/kontrak/' + file_name;

                // Loaded via <script> tag, create shortcut to access PDF.js exports.
                var pdfjsLib = window['pdfjs-dist/build/pdf'];

                // The workerSrc property shall be specified.
                pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

                // Asynchronous download of PDF
                var loadingTask = pdfjsLib.getDocument(url);
                loadingTask.promise.then(function(pdf) {
                    console.log('PDF loaded');
                  
                    // Fetch the first page
                    var pageNumber = 1;
                    pdf.getPage(pageNumber).then(function(page) {
                        console.log('Page loaded');
                        
                        var scale = 1.5;
                        var viewport = page.getViewport({scale: scale});

                        // Prepare canvas using PDF page dimensions
                        var canvas = document.getElementById('the-canvas');
                        var context = canvas.getContext('2d');
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;

                        // Render PDF page into canvas context
                        var renderContext = {
                          canvasContext: context,
                          viewport: viewport
                        };
                        var renderTask = page.render(renderContext);
                        renderTask.promise.then(function () {
                          console.log('Page rendered');
                        });
                    });
                }, function (reason) {
                    // PDF loading error
                    console.error(reason);
                });
                $('#file-modal').modal('show');
            })
            
        </script>

    </body>
</html>