<head>
    <meta charset="utf-8" />
    <title>DMS | Sisfo Tel-U</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/') ?>images/telu.png">

    <!-- App css -->
    <link href="<?php echo base_url('assets') ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets') ?>/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets') ?>/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets') ?>/css/style_cl.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets') ?>/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!--Footable-->
    <link href="<?php echo base_url('assets') ?>/plugins/footable/css/footable.core.css" rel="stylesheet">

    <!-- Select -->
    <link href="<?php echo base_url('assets') ?>/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets') ?>/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert css -->
    <link href="<?php echo base_url('assets') ?>/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

    <!-- Tooltipster css -->
    <link rel="stylesheet" href="<?php echo base_url('assets') ?>/plugins/tooltipster/tooltipster.bundle.min.css">

    <!-- Dropzone css -->
    <link href="<?php echo base_url('assets') ?>/plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" />

    <script src="<?php echo base_url('assets') ?>/js/modernizr.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/fileupload.css">

</head>