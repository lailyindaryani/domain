<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('include/sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <?php $this->load->view('include/topbar'); ?>
                <!-- Top Bar End -->

                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <!-- Add modal content -->
                        <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-body">
                                        <h2 class="text-uppercase text-center m-b-30">
                                            <a href="<?php echo base_url() ?>" class="text-success">
                                                <span><img src="<?php echo base_url('assets') ?>/images/logo.png" alt="" height="28"></span>
                                            </a>
                                        </h2>

                                        <form id="add-form" action="#">
                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="username">Kategori</label>
                                                    <input class="form-control" type="text" id="add_type" required="" placeholder="Kategori">
                                                </div>
                                            </div>

                                            <div class="form-group account-btn text-center m-t-10">
                                                <div class="col-12">
                                                    <button class="btn w-lg btn-rounded btn-primary waves-effect waves-light" id="add-button" type="submit">Submit</button>
                                                </div>
                                            </div>
                                        </form>


                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <!-- Edit modal content -->
                        <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-body">
                                        <h2 class="text-uppercase text-center m-b-30">
                                            <a href="<?php echo base_url() ?>" class="text-success">
                                                <span><img src="<?php echo base_url('assets') ?>/images/logo.png" alt="" height="28"></span>
                                            </a>
                                        </h2>

                                        <form id="edit-form" action="#">
                                            <input type="hidden" name="edit_id" id="edit_id">
                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="username">Kategori</label>
                                                    <input class="form-control" type="text" id="edit_type" required="" placeholder="Kategori">
                                                </div>
                                            </div>

                                            <div class="form-group account-btn text-center m-t-10">
                                                <div class="col-12">
                                                    <button class="btn w-lg btn-rounded btn-primary waves-effect waves-light" id="edit-button" type="submit">Submit</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <button type="button" class="btn btn-custom btn-sm btn-rounded w-md waves-effect waves-light float-right" data-toggle="modal" data-target="#add-modal">Add New</button>
                                    <h4 class="m-t-0 header-title">Domain Kategori</h4>
                                    <br>
                                    <table class="table table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kategori</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1; foreach ($type as $key): ?>
                                                <tr>
                                                    <th scope="row"><?php echo $no++ ?></th>
                                                    <td><?php echo $key->type ?></td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon waves-effect waves-light btn-primary edit" title="Click to edit" data-toggle="modal" data-target="#edit-modal" data-id="<?php echo $key->id_type ?>" data-type="<?php echo $key->type ?>"> <i class="fa fa-edit"></i> </button>
                                                        <button type="button" class="btn btn-icon waves-effect waves-light btn-warning delete" title="Click to delete" data-id="<?php echo $key->id_type ?>"> <i class="fa fa-trash-o"></i> </button>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php $this->load->view('include/footer'); ?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script> 
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?php echo base_url('assets') ?>/plugins/sweet-alert/sweetalert2.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/pages/jquery.sweet-alert.init.js"></script>

        <!-- Tooltipster js -->
        <script src="<?php echo base_url('assets') ?>/plugins/tooltipster/tooltipster.bundle.min.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>

        <script>
            $('.edit').tooltipster();
            $('.delete').tooltipster();

            $('.edit').click(function() {
                $("#edit_id").val($(this).data("id"));
                $("#edit_type").val($(this).data("type"));
            });

            $('#add-button').click(function() {
                type = $('#add_type').val();

                if (type == '') {} else {
                    $.ajax({
                        url: '<?php echo base_url('domains/type_add') ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {'type': type},
                    })
                    .done(function() {
                        $('#add-modal').modal('hide');
                        swal({
                            title: 'Good job!',
                            text: 'A record have been added!',
                            type: 'success',
                            timer: 3000,
                            confirmButtonClass: 'btn btn-confirm'
                        }).then(function() {
                            location.reload();
                        });
                    })
                    .fail(function() {
                        swal({
                            title: 'Error',
                            text: "Adding record failed!",
                            type: 'error',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        });
                    });
                }
            })

            $('#edit-button').click(function() {
                id = $('#edit_id').val();
                type = $('#edit_type').val();

                $.ajax({
                    url: '<?php echo base_url('domains/type_edit') ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {'id': id, 'type': type},
                })
                .done(function() {
                    $('#edit-modal').modal('hide');
                    swal({
                        title: 'Good job!',
                        text: 'A record have been updated!',
                        type: 'success',
                        // timer: 3000,
                        confirmButtonClass: 'btn btn-confirm'
                    }).then(function() {
                        location.reload();
                    });
                })
                .fail(function(e) {
                    console.log(e);
                    swal({
                        title: 'Error',
                        text: "Updating record failed!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                });
            })

            $('.delete').click(function() {
                id = $(this).data("id");
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    $.ajax({
                        url: '<?php echo base_url('domains/type_delete') ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {'id': id},
                    })
                    .done(function() {
                        swal({
                            title: 'Deleted !',
                            text: "Your record has been deleted",
                            type: 'success',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        }).then(function() {
                            location.reload();
                        });
                    })
                    .fail(function() {
                        swal({
                            title: 'Error',
                            text: "Deleting record failed!",
                            type: 'error',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        });
                    });
                    
                });
            })
        </script>

    </body>
</html>