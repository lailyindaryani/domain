 <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
 	<title>CodeIgniter Ajax File Upload</title>
 	<script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
 	<script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
</head>
<body>
 <nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
   <div class="navbar-header">
    <a class="navbar-brand" href="https://www.tutorialswb.com/">TutorialsWB</a>
    <ul class="nav navbar-nav">
     <li class="active"><a href="#">CodeIgniter Ajax File Upload</a></li>
    </ul>
   </div>
  </div>
 </nav>
 <div class="container">
  <div class="row">
   <div class="col-sm-4">
    <h3>File Upload Form</h3>
    <form id="uploadForm">
     <div class="form-group">
      <label>File:</label>
      <input type="file" name="upload" id="file">
     </div>
     <button type="submit" class="btn btn-primary">Save</button>
    </form>
    <div id="responseDiv" class="alert text-center" style="margin-top:20px; display:none;">
     <button type="button" class="close" id="clearMsg"><span aria-hidden="true">&times;</span></button>
     <span id="message"></span>
    </div>  
   </div>
   <div class="col-sm-8">
    <table class="table table-bordered table-striped">
     <thead>
      <tr>
       <th>ID</th>
       <th>Filename</th>
       <th>Preview</th>
      </tr>
     </thead>
     <tbody id="tbody">
     </tbody>
    </table>
   </div>
  </div>
 </div>
 <script type="text/javascript">
  $(document).ready(function(){
   fetchTable();

   $('#uploadForm').submit(function(e){
    e.preventDefault();
    var url = '<?php echo base_url(); ?>';

    var reader = new FileReader();
    reader.readAsDataURL(document.getElementById('file').files[0]);

    var formdata = new FormData();
    formdata.append('file', document.getElementById('file').files[0]);
    $.ajax({
     method: 'POST',
     contentType: false,
     cache: false,
     processData: false,
     data: formdata,
     dataType: 'json',
     url: url + 'domains/file_upload',
     success: function(response){
      console.log(response);
      if(response.error){
       $('#responseDiv').removeClass('alert-success').addClass('alert-danger').show();
       $('#message').html(response.message);
      }
      else{
       $('#responseDiv').removeClass('alert-danger').addClass('alert-success').show();
       $('#message').html(response.message);
       fetchTable();
       $('#uploadForm')[0].reset();
      }
     }
    });
   });

   $('#clearMsg').click(function(){
    $('#responseDiv').hide();
   });

  });
  function fetchTable(){
   var url = '<?php echo base_url(); ?>';
   $.ajax({
    method: 'POST',
    url: url + 'domains/file_upload',
    success: function(response){
     $('#tbody').html(response);
    }
   });
  }
 </script>
</body>
</html> 