<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body class="account-pages">

        <!-- Begin page -->
        <div class="accountbg" style="background: url('<?php echo base_url('assets') ?>/images/business.jpg');background-size: cover;background-position: center;"></div>

        <div class="wrapper-page account-page-full">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="index.html" class="text-success">
                                    <span><img src="<?php echo base_url('assets') ?>/images/logo.png" alt="" height="26"></span>
                                </a>
                            </h2>

                            <?php echo $this->session->flashdata('login_gagal'); ?>

                            <form class="" action="<?php echo base_url('auth/login') ?>" method="post">

                                <div class="form-group m-b-20 row">
                                    <div class="col-12">
                                        <label for="emailaddress">Username SSO</label>
                                        <input class="form-control" type="text" id="userName" required="" placeholder="Enter your username" name="username">
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <!-- <a href="page-recoverpw.html" class="text-muted float-right"><small>Forgot your password?</small></a> -->
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" required="" id="password" placeholder="Enter your password" name="password">
                                    </div>
                                </div>

                                <!-- <div class="form-group row m-b-20">
                                    <div class="col-12">

                                        <div class="checkbox checkbox-custom">
                                            <input id="remember" type="checkbox" checked="">
                                            <label for="remember">
                                                Remember me
                                            </label>
                                        </div>

                                    </div>
                                </div> -->

                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Sign In</button>
                                    </div>
                                </div>

                            </form>

                            <!-- <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Don't have an account? <a href="page-register.html" class="text-dark m-l-5"><b>Sign Up</b></a></p>
                                </div>
                            </div> -->

                        </div>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">2019 © Konten - Telkom University</p>
            </div>

        </div>



        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>

    </body>
</html>