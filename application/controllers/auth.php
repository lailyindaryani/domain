<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_crud');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function login()
	{
		$data = array(
			'username_sso' => $this->input->post('username'),
			'password' => md5($this->input->post('password'))
		);

		$userdata = $this->m_crud->login($data);

		if (empty($userdata)) {
			$notif = 	'<div class="alert alert-warning alert-dismissible fade show rounded mb-20" role="alert">
                            <strong>Gagal!</strong> Username or password not match.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>';
			$this->session->set_flashdata('login_gagal', $notif);
			$this->index();
		} else {

			$array = array(
				'username' => $userdata->username_sso,
				'name' => $userdata->full_name,
				'email' => $userdata->email,
			);
			
			$this->session->set_userdata( $array );

			redirect('dashboard');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->load->view('logout');
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */