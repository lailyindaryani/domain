<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('include/sidebar'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <?php $this->load->view('include/topbar'); ?>
                <!-- Top Bar End -->

                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <!-- Signup modal content -->
                        <div id="signup-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-body">
                                        <h2 class="text-uppercase text-center m-b-30">
                                            <a href="index.html" class="text-success">
                                                <span><img src="assets/images/logo.png" alt="" height="28"></span>
                                            </a>
                                        </h2>


                                        <form id="add-form" action="#">

                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="username">Full Name</label>
                                                    <input class="form-control" type="text" id="add_fullname" required="" placeholder="Full Name">
                                                </div>
                                            </div>

                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="emailaddress">Email address</label>
                                                    <input class="form-control" type="email" id="add_emailaddress" required="" placeholder="Email">
                                                </div>
                                            </div>

                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="username">Username</label>
                                                    <input class="form-control" type="text" id="add_username" required="" placeholder="Username">
                                                </div>
                                            </div>

                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="password">Password</label>
                                                    <input class="form-control" type="password" required="" id="add_password" placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="form-group account-btn text-center m-t-10">
                                                <div class="col-12">
                                                    <button class="btn w-lg btn-rounded btn-primary waves-effect waves-light" id="add-button" type="submit">Submit</button>
                                                </div>
                                            </div>

                                        </form>


                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <!-- Edit modal content -->
                        <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-body">
                                        <h2 class="text-uppercase text-center m-b-30">
                                            <a href="index.html" class="text-success">
                                                <span><img src="<?php echo base_url('assets') ?>/images/logo.png" alt="" height="28"></span>
                                            </a>
                                        </h2>

                                        <form id="edit-form" action="#">

                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="username">Full Name</label>
                                                    <input class="form-control" type="text" id="edit_fullname" required="" placeholder="Full Name">
                                                </div>
                                            </div>

                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="emailaddress">Email address</label>
                                                    <input class="form-control" type="email" id="edit_emailaddress" required="" placeholder="Email">
                                                </div>
                                            </div>

                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="username">Username</label>
                                                    <input class="form-control" type="text" id="edit_username" required="" placeholder="Username" readonly="">
                                                </div>
                                            </div>

                                            <div class="form-group m-b-25">
                                                <div class="col-12">
                                                    <label for="password">Password</label>
                                                    <input class="form-control" type="password" required="" id="edit_password" placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="form-group account-btn text-center m-t-10">
                                                <div class="col-12">
                                                    <button class="btn w-lg btn-rounded btn-primary waves-effect waves-light" id="edit-button" type="submit">Submit</button>
                                                </div>
                                            </div>

                                        </form>


                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <button type="button" class="btn btn-custom btn-sm btn-rounded w-md waves-effect waves-light float-right" data-toggle="modal" data-target="#signup-modal">Add New</button>
                                    <h4 class="m-t-0 header-title">Administrator</h4>
                                    <p><br></p>
                                    <table class="table table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Full Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1; foreach ($admin as $key): ?>
                                                <tr>
                                                    <th scope="row"><?php echo $no++ ?></th>
                                                    <td><?php echo $key->full_name ?></td>
                                                    <td><?php echo $key->username_sso ?></td>
                                                    <td><?php echo $key->email ?></td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon waves-effect waves-light btn-primary edit" title="Click to edit" data-toggle="modal" data-target="#edit-modal" data-name="<?php echo $key->full_name ?>" data-username="<?php echo $key->username_sso ?>" data-email="<?php echo $key->email ?>"> <i class="fa fa-edit"></i> </button>
                                                        <button type="button" class="btn btn-icon waves-effect waves-light btn-warning delete" title="Click to delete" data-id="<?php echo $key->username_sso ?>"> <i class="fa fa-trash-o"></i> </button>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php $this->load->view('include/footer'); ?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script> 
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?php echo base_url('assets') ?>/plugins/sweet-alert/sweetalert2.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/pages/jquery.sweet-alert.init.js"></script>

        <!-- Tooltipster js -->
        <script src="<?php echo base_url('assets') ?>/plugins/tooltipster/tooltipster.bundle.min.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>

        <script>
            $('.edit').tooltipster();
            $('.delete').tooltipster();

            $('.edit').click(function() {
                $("#edit_fullname").val($(this).data("name"));
                $("#edit_username").val($(this).data("username"));
                $('#edit_emailaddress').val($(this).data("email"));
            });

            $('#add-button').click(function() {
                fullname = $('#add_fullname').val();
                email = $('#add_emailaddress').val();
                username = $('#add_username').val();
                password = $('#add_password').val();

                $.ajax({
                    url: '<?php echo base_url('admin/add') ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {'fullname': fullname, 'email':email, 'username': username, 'password': password},
                })
                .done(function() {
                    $('#signup-modal').modal('hide');
                    swal({
                        title: 'Good job!',
                        text: 'A record have been added!',
                        type: 'success',
                        timer: 3000,
                        confirmButtonClass: 'btn btn-confirm'
                    }).then(function() {
                        location.reload();
                    });
                })
                .fail(function() {
                    swal({
                        title: 'Error',
                        text: "Adding record failed!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                });
            })

            $('#edit-button').click(function() {
                fullname = $('#edit_fullname').val();
                email = $('#edit_emailaddress').val();
                username = $('#edit_username').val();
                password = $('#edit_password').val();

                $.ajax({
                    url: '<?php echo base_url('admin/edit') ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {'fullname': fullname, 'email': email, 'username': username, 'password': password},
                })
                .done(function() {
                    $('#edit-modal').modal('hide');
                    swal({
                        title: 'Good job!',
                        text: 'A record have been updated!',
                        type: 'success',
                        timer: 3000,
                        confirmButtonClass: 'btn btn-confirm'
                    }).then(function() {
                        location.reload();
                    });
                })
                .fail(function(res) {
                    console.log(res);
                    swal({
                        title: 'Error',
                        text: "Updating record failed!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                });
            })

            $('.delete').click(function() {
                id = $(this).data("id");
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    $.ajax({
                        url: '<?php echo base_url('admin/delete') ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {'id': id},
                    })
                    .done(function(res) {
                        console.log(res);
                        swal({
                            title: 'Deleted !',
                            text: "Your record has been deleted",
                            type: 'success',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        }).then(function() {
                            location.reload();
                        });
                    })
                    .fail(function() {
                        swal({
                            title: 'Error',
                            text: "Deleting record failed!",
                            type: 'error',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        });
                    });
                    
                });
            })
        </script>

    </body>
</html>