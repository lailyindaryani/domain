<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('include/sidebar'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <?php $this->load->view('include/topbar'); ?>
                <!-- Top Bar End -->

                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <button type="button" class="btn btn-custom btn-rounded w-md waves-effect waves-light float-right" onclick="window.location.href='<?php echo base_url('pic/add') ?>'">New Domain</button>
                                    <h4 class="m-t-0 header-title">List PIC</h4>
                                    <br>

                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-12 text-sm-center form-inline">
                                                <label class="form-inline mr-3">
                                                    Show:
                                                    <select id="demo-show-entries" class="form-control form-control-sm ml-1 mr-1">
                                                        <option value="5">5</option>
                                                        <option value="10">10</option>
                                                        <option value="15">15</option>
                                                        <option value="20">20</option>
                                                    </select>
                                                </label>
                                                <div class="form-group mr-2">
                                                    <select id="demo-foo-filter-status" class="custom-select">
                                                        <option value="">Show all</option>
                                                        <option value="active">Active</option>
                                                        <option value="disabled">Disabled</option>
                                                        <option value="suspended">Suspended</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <input id="demo-foo-search" type="text" placeholder="Search" class="form-control" autocomplete="on">
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <table id="table-domain" class="table m-b-0 table-bordered toggle-arrow-tiny" data-page-size="5">
                                        <thead>
                                            <tr>
                                                <th> Nama Admin </th>
                                                <th> Username </th>
                                                <th> e-Mail </th>
                                                <th> No. HP </th>
                                                <th> Jabatan </th>
                                                <th> Lokasi Kerja </th>
                                                <th> Domain </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($pic as $key): ?>
                                                <tr>
                                                    <td><?php echo $key->nama_admin ?></td>
                                                    <td><?php echo $key->username_sso ?></td>
                                                    <td><?php echo $key->email ?></td>
                                                    <td><?php echo $key->no_hp ?></td>
                                                    <td><?php echo $key->jabatan ?></td>
                                                    <td><?php echo $key->unit ?></td>
                                                    <td>198.223.212.5</td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon waves-effect btn-primary btn-rounded view_contract" title="Edit this domain" onclick="location.href='<?php echo base_url('pic/edit/') . $key->username_sso; ?>'"> <i class="fa fa-pencil"></i> </button>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                        <tfoot>
                                            <tr class="active">
                                                <td colspan="15">
                                                    <div class="text-right">
                                                        <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php $this->load->view('include/footer'); ?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <!--FooTable-->
        <script src="<?php echo base_url('assets') ?>/plugins/footable/js/footable.all.min.js"></script>

        <!-- Tooltipster js -->
        <script src="<?php echo base_url('assets') ?>/plugins/tooltipster/tooltipster.bundle.min.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>

        <script>
            var domain = $('#table-domain');
            domain.footable();

            $('#demo-show-entries').change(function (e) {
                e.preventDefault();
                var pageSize = $(this).val();
                domain.data('page-size', pageSize);
                domain.trigger('footable_filter');
            });

            domain.footable().on('footable_filtering', function (e) {
                var selected = $('#demo-foo-filter-status').find(':selected').val();
                e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                e.clear = !e.filter;
            });

            // Filter status
            $('#demo-foo-filter-status').change(function (e) {
                e.preventDefault();
                domain.trigger('footable_filter', {filter: $(this).val()});
            });

            // Search input
            $('#demo-foo-search').on('input', function (e) {
                e.preventDefault();
                domain.trigger('footable_filter', {filter: $(this).val()});
            });

            $('.view_contract').tooltipster();
            $('.set_pic').tooltipster();
        </script>

    </body>
</html>