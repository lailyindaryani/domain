<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_crud extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_table($table)
	{
		return $this->db->get($table);
	}

	public function get_where($where, $table)
	{
		$this->db->where($where);
		// $this->db->from($table);
		return $this->db->get($table);
		// return $this->db->get_compiled_select();
	}

	public function add($object, $table)
	{
		$this->db->insert($table, $object);
		return $this->db->affected_rows();
	}

	public function edit($object, $where, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $object);
		// return $this->db->get_compiled_string();
		return $this->db->affected_rows();
	}

	public function delete($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
		return $this->db->affected_rows();
	}

	public function login($data)
	{
		$this->db->select('username_sso, full_name, email');
		$this->db->where($data);
		return $this->db->get('admin')->row();
	}
}

/* End of file m_crud.php */
/* Location: ./application/models/m_crud.php */