<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pic extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('username') == false) {
			redirect('auth');
		}
		$this->load->model('m_crud');
		$this->load->model('m_join');
	}

	public function index()
	{
		$data['pic'] = $this->m_join->pic_unit()->result();
		$this->load->view('pic/pic_dashboard', $data);
	}

	public function add()
	{
		$data['unit'] = $this->m_crud->get_table('lokasi_kerja')->result();
		$this->load->view('pic/pic_add', $data);
	}

	public function add_process()
	{
		$username = $this->input->post('username');
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$jabatan = $this->input->post('jabatan');
		$lokasi = $this->input->post('lokasi');

		$data = array(
			'username_sso' => $username,
			'nama_admin' => $fullname,
			'email' => $email,
			'no_hp' => $hp,
			'jabatan' => $jabatan,
			'lokasi_kerja' => $lokasi
		);

		$insert = $this->m_crud->add($data, 'pic');

		echo json_encode($insert);
	}

	public function edit($id)
	{
		$data['unit'] = $this->m_crud->get_table('lokasi_kerja')->result();

		$where = array('username_sso' => $id);
		$data['pic'] = $this->m_crud->get_where($where, 'pic')->row();

		$this->load->view('pic/pic_edit', $data);
	}

	public function edit_process()
	{
		$username = $this->input->post('username');
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$jabatan = $this->input->post('jabatan');
		$lokasi = $this->input->post('lokasi');

		$data = array(
			'nama_admin' => $fullname,
			'email' => $email,
			'no_hp' => $hp,
			'jabatan' => $jabatan,
			'lokasi_kerja' => $lokasi
		);
		$where = array('username_sso' => $username);

		$update = $this->m_crud->edit($data, $where, 'pic');

		echo json_encode($update);
	}

	public function department()
	{
		$data['unit'] = $this->m_crud->get_table('lokasi_kerja')->result();
		$this->load->view('pic/pic_department', $data);
	}

	public function department_add()
	{
		$unit = $this->input->post('unit');
		$data = array('unit' => $unit);
		$insert = $this->m_crud->add($data, "lokasi_kerja");

		echo json_encode($insert);
	}

	public function department_edit()
	{
		$id = $this->input->post('id');
		$unit = $this->input->post('unit');

		$data = array('unit' => $unit);
		$where = array('id_lokasi' => $id);

		$update = $this->m_crud->edit($data, $where, "lokasi_kerja");

		echo json_encode($update);
	}

	public function department_delete()
	{
		$id = $this->input->post('id');

		$where = array('id_lokasi' => $id);

		$delete = $this->m_crud->delete($where, "lokasi_kerja");

		echo json_encode($delete);
	}

	public function get_pic_by_id()
	{
		$id = $this->input->post('id');

		$data_pic = $this->m_join->pic_unit_by_id($id)->row();

		echo json_encode($data_pic);
	}

}

/* End of file pic.php */
/* Location: ./application/controllers/pic.php */