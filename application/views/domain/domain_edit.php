<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('include/sidebar'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <?php $this->load->view('include/topbar'); ?>
                <!-- Top Bar End -->

                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                            <?php if ($this->session->flashdata('upload') == 'error'): ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    This is a danger alert—check it out!
                                </div>
                                <?php endif ?>
                                    <div class="card-box">

                                    <h4 class="m-t-0 header-title">Domain Information</h4>
                                    <hr>

                                    <form action="<?php echo base_url('domains/edit_process') ?>" method="post" id="form_add" enctype="multipart/form-data">

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Domain Name</label>
                                            <div class="col-10">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Domain Name" aria-label="Domain Name" aria-describedby="basic-addon1" id="domain_name" name="domain_name" value="<?php echo $domain->nama_domain ?>" readonly>
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">.telkomuniversity.ac.id</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Domain Type</label>
                                            <div class="col-10">
                                                <select class="selectpicker" data-live-search="true" data-style="btn-custom" id="domain_type" title="Select option..." name="domain_type">
                                                    <?php foreach ($type as $key): ?>
                                                        <option value="<?php echo $key->id_type ?>" <?php if ($key->id_type == $domain->id_type): ?>
                                                            selected
                                                        <?php endif ?>><?php echo $key->type ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Domain Location</label>
                                            <div class="col-10">
                                                <select class="selectpicker" data-live-search="true" data-style="btn-custom" id="domain_lokasi" title="Select option..." name="domain_lokasi">
                                                    <?php foreach ($server as $key): ?>
                                                        <option value="<?php echo $key->id_lokasi ?>" <?php if ($key->id_lokasi == $domain->id_lokasi): ?>
                                                            selected
                                                        <?php endif ?>><?php echo $key->jenis ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">User Domain</label>
                                            <div class="col-10">
                                                <input type="text" class="form-control" placeholder="User Domain" id="user_domain" name="user_domain" value="<?php echo $domain->user_domain ?>">
                                            </div>
                                        </div>

                                        <h4 class="m-t-0 header-title">PIC Information</h4>

                                        <hr>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Username PIC</label>
                                            <div class="col-10">
                                                <select class="selectpicker" data-live-search="true" data-style="btn-custom" id="pic" title="Select option..." name="username">
                                                    <?php foreach ($pic as $key): ?>
                                                        <option value="<?php echo $key->username_sso ?>" <?php if ($key->username_sso == $domain->username_sso): ?>
                                                            selected
                                                        <?php endif ?>><?php echo $key->username_sso ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                        <dl class="row mb-0">
                                            <dt class="col-sm-2">Full Name</dt>
                                            <dd class="col-sm-10">
                                                <p id="fullname">Full Name</p>
                                            </dd>

                                            <dt class="col-sm-2">Email</dt>
                                            <dd class="col-sm-10">
                                                <p id="email">Email</p>
                                            </dd>

                                            <dt class="col-sm-2">No. HP</dt>
                                            <dd class="col-sm-10">
                                                <p id="hp">No. HP</p>
                                            </dd>

                                            <dt class="col-sm-2">Department</dt>
                                            <dd class="col-sm-10">
                                                <p id="jabatan">Jabatan dan Lokasi Kerja</p>
                                            </dd>
                                        </dl>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">File Contract</label>
                                            <div class="col-10">
                                                <input type="file" class="filestyle" data-buttonbefore="true" data-btnClass="btn-custom" name="userfile">
                                            </div>
                                        </div>

                                        <hr>

                                        <!-- <button type="button" class="btn btn-secondary waves-effect waves-light" id="edit" data-id="<?php echo $domain->username_sso ?>">Edit PIC</button> -->
                                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="add">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php $this->load->view('include/footer'); ?>

            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>
        <script src="<?php echo base_url('assets') ?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?php echo base_url('assets') ?>/plugins/sweet-alert/sweetalert2.min.js"></script>

        <!-- Select2 -->
        <script src="<?php echo base_url('assets') ?>/plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>

        <script>
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });

            $('#sign_date').datepicker().on('change', function() {
                sign = $('#sign_date').datepicker('getDate');;
                sign.setFullYear(sign.getFullYear() + 1);
                sign.setDate(sign.getDate() + 1);

                $('#expired_date').val(sign.toISOString().slice(0,10));
            });

            $('#selectpic').on('change', function() {
                this.value;
                if(this.value == 'new') {
                    $("#exist-pic").hide();
                    $("#new-pic").show();
                } else {
                    $("#exist-pic").show();
                    $("#new-pic").hide();
                }
            });

            $('#datepicker').datepicker('setDate', 'today');

            pic = $('#pic').val();
            $.ajax({
               url: '<?php echo base_url('pic/get_pic_by_id') ?>',
               type: 'post',
               dataType: 'json',
               data: {'id': pic},
            })
            .done(function(res) {
                $("#username").text(res.username_sso);
                $('#fullname').text(res.nama_admin);
                $('#email').text(res.email);
                $('#hp').text(res.no_hp);
                $('#jabatan').text(res.jabatan + " - " + res.unit)
            })
            .fail(function() {
                swal({
                    title: 'Error',
                    text: "Something error! Please contact the administrator!",
                    type: 'error',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                });
            });

            $('#pic').on("change", function(e) { 
                id = $(this).val();
               
                $.ajax({
                   url: '<?php echo base_url('pic/get_pic_by_id') ?>',
                   type: 'post',
                   dataType: 'json',
                   data: {'id': id},
                })
                .done(function(res) {
                    $("#username").text(res.username_sso);
                    $('#fullname').text(res.nama_admin);
                    $('#email').text(res.email);
                    $('#hp').text(res.no_hp);
                    $('#jabatan').text(res.jabatan + " - " + res.unit)
                })
                .fail(function() {
                    swal({
                        title: 'Error',
                        text: "Something error! Please contact the administrator!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                });
            });

            // $('#edit').click(function() {
            //     id = $(this).val();
            //     console.log(id);

            //     swal({
            //         title: 'Redirecting to PIC edit page.',
            //         text: "Do you want to continue and save the record?",
            //         type: 'warning',
            //         showCancelButton: true,
            //         confirmButtonText: 'Save and continue!',
            //         cancelButtonText: 'Just continue',
            //         confirmButtonClass: 'btn btn-success mt-2',
            //         cancelButtonClass: 'btn btn-danger ml-2 mt-2',
            //         buttonsStyling: false
            //     }).then(function () {
            //         swal({
            //             title: 'Deleted !',
            //             text: "Your file has been deleted",
            //             type: 'success',
            //             confirmButtonClass: 'btn btn-confirm mt-2'
            //         }
            //         )
            //     }, function (dismiss) {
            //         // dismiss can be 'cancel', 'overlay',
            //         // 'close', and 'timer'
            //         if (dismiss === 'cancel') {
            //             swal({
            //                 title: 'Cancelled',
            //                 text: "Your imaginary file is safe :)",
            //                 type: 'error',
            //                 confirmButtonClass: 'btn btn-confirm mt-2'
            //             }
            //             )
            //         }
            //     })
            // });
        </script>

    </body>
</html>



