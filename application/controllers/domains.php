<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class domains extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('username') == false) {
			redirect('auth');
		}
		$this->load->model('m_crud');
		$this->load->model('m_join');
		$this->load->helper('form');
	}

	public function index()
	{
		$data['domain'] = $this->m_join->domain_pic_unit_active()->result();
		$data['pic'] = $this->m_join->pic_unit()->result();
		$data['error'] = '';

		$this->load->view('domain/domain_dashboard', $data);
	}

	public function add()
	{
		$data['unit'] = $this->m_crud->get_table('lokasi_kerja')->result();
		$data['type'] = $this->m_crud->get_table('domain_type')->result();
		$data['server'] = $this->m_crud->get_table('lokasi_server')->result();
		$data['pic'] = $this->m_crud->get_table('pic')->result();
		$this->load->view('domain/domain_add', $data);
	}

	public function add_process()
	{
		$name = $this->input->post('domain_name');
		$type = $this->input->post('domain_type');
		$lokasi = $this->input->post('domain_lokasi');
		$user_domain = $this->input->post('user_domain');
		$username = $this->input->post('username');
		$created_date = $this->input->post('created_date');
		$signed_date = $this->input->post('signed_date');
		$expired_date = $this->input->post('expired_date');
		$pic = $this->input->post('pic');
		$file_name = '';

		if ($pic === "new") {
			$username = $this->input->post('newUsername');
			$fullname = $this->input->post('newFullname');
			$email = $this->input->post('newEmail');
			$hp = $this->input->post('newHp');
			$jabatan = $this->input->post('newJabatan');
			$lokasi = $this->input->post('newLokasi');

			$datapic = array(
				'username_sso' => $username,
				'nama_admin' => $fullname,
				'email' => $email,
				'no_hp' => $hp,
				'jabatan' => $jabatan,
				'lokasi_kerja' => $lokasi
			);

			$insertpic = $this->m_crud->add($datapic, 'pic');
		}

		$config['upload_path']          = './kontrak/';
		$config['allowed_types']        = 'gif|jpg|png|pdf';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
        {
            $this->session->set_flashdata('upload', 'error');
			$this->load->view('domain/domain_add');
			// echo $this->upload->display_errors();
        }
        else
        {
        	$result = $this->upload->data();
        	$file_name = $result['file_name'];
        }

		$data = array(
			'nama_domain' => $name,
			'username_sso' => $username,
			'id_lokasi' => $lokasi,
			'id_type' => $type,
			'status_domain' => 1,
			'user_domain' => $user_domain,
			'file_kontrak' => $file_name,
			'created_date' => $created_date,
			'sign_date' => $signed_date,
			'expired_date' => $expired_date
		);
		
		$insert = $this->m_crud->add($data, 'domain');

		if ($signed_date != '') {
			$this->update_history($name, $signed_date, $expired_date);
		}

		redirect('domains','refresh');
	}

	public function edit($id)
	{
		$where = array('nama_domain' => $id);
		$data['domain'] = $this->m_crud->get_where($where, "domain")->row();
		$data['type'] = $this->m_crud->get_table('domain_type')->result();
		$data['server'] = $this->m_crud->get_table('lokasi_server')->result();
		$data['pic'] = $this->m_crud->get_table('pic')->result();

		$this->load->view('domain/domain_edit', $data);
	}

	public function edit_process()
	{
		$name = $this->input->post('domain_name');
		$type = $this->input->post('domain_type');
		$lokasi = $this->input->post('domain_lokasi');
		$user_domain = $this->input->post('user_domain');
		$username = $this->input->post('username');
		$created_date = $this->input->post('created_date');
		$file_name = '';

		$config['upload_path']          = './kontrak/';
		$config['allowed_types']        = 'gif|jpg|png|pdf';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
        {
            $this->session->set_flashdata('upload', 'error');
			$this->load->view('domain/domain_edit');
			echo $this->upload->display_errors();
        }
        else
        {
        	$result = $this->upload->data();
        	$file_name = $result['file_name'];
        }

		$data = array(
			'username_sso' => $username,
			'id_lokasi' => $lokasi,
			'id_type' => $type,
			'user_domain' => $user_domain,
			'file_kontrak' => $file_name,
		);

		$where = array('nama_domain' => $name);

		$update = $this->m_crud->edit($data, $where, 'domain');
		redirect('domains','refresh');
	}

	public function set_pic()
	{
		$pic = $this->input->post('pic');
		$id = $this->input->post('id');

		$where = array('nama_domain' => $id);
		$data = array('username_sso' => $pic);

		$update = $this->m_crud->edit($data, $where, 'domain');

		echo json_encode($update);
	}

	public function set_date()
	{
		$id = $this->input->post('id');
		$sign_date = $this->input->post('sign_date');
		$expired_date = $this->input->post('expired_date');

		$where = array('nama_domain' => $id);
		$data = array(
			'sign_date' => $sign_date,
			'expired_date' => $expired_date
		);

		$update = $this->m_crud->edit($data, $where, 'domain');

		if ($signed_date != '') {
			$this->update_history($id, $sign_date, $expired_date);
		}

		echo json_encode($update);
	}

	public function suspend()
	{
		$id = $this->input->post('id');

		$where = array('nama_domain' => $id);
		$data = array('status_domain' => 2);

		$update = $this->m_crud->edit($data, $where, 'domain');

		echo json_encode($update);
	}

	public function delete()
	{
		$id = $this->input->post('id');

		$where = array('nama_domain' => $id);
		$data = array('status_domain' => 3);

		$update = $this->m_crud->edit($data, $where, 'domain');

		echo json_encode($update);
	}

	public function perm_delete()
	{
		$id = $this->input->post('id');
		$where = array('nama_domain' => $id);

		$delete = $this->m_crud->delete($where, 'domain');

		echo json_encode($delete);
	}

	public function nonaktif()
	{
		$data['domain'] = $this->m_join->domain_pic_unit_nonaktif()->result();
		$this->load->view('domain/domain_nonaktif', $data);
	}

	public function activation($id)
	{
		$data['domain'] = $this->m_join->domain_full($id)->row();
		$data['type'] = $this->m_crud->get_table('domain_type')->result();
		$data['server'] = $this->m_crud->get_table('lokasi_server')->result();
		$data['pic'] = $this->m_crud->get_table('pic')->result();
		$this->load->view('domain/domain_activation', $data);

	}

	public function activation_process()
	{
		$name = $this->input->post('domain_name');
		$username = $this->input->post('username');
		$pic = $this->input->post('pic');
		$signed_date = $this->input->post('signed_date');
		$expired_date = $this->input->post('expired_date');
		$file_name = '';

		if ($pic === "new") {
			$username = $this->input->post('newUsername');
			$fullname = $this->input->post('newFullname');
			$email = $this->input->post('newEmail');
			$hp = $this->input->post('newHp');
			$jabatan = $this->input->post('newJabatan');
			$lokasi = $this->input->post('newLokasi');

			$datapic = array(
				'username_sso' => $username,
				'nama_admin' => $fullname,
				'email' => $email,
				'no_hp' => $hp,
				'jabatan' => $jabatan,
				'lokasi_kerja' => $lokasi
			);

			$insertpic = $this->m_crud->add($datapic, 'pic');
		}

		$config['upload_path']          = './kontrak/';
		$config['allowed_types']        = 'gif|jpg|png|pdf';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
        {
            $this->session->set_flashdata('upload', 'error');
			$this->load->view('domain/domain_nonaktif');
			// echo $this->upload->display_errors();
        }
        else
        {
        	$result = $this->upload->data();
        	$file_name = $result['file_name'];
        }

		$data = array(
			'username_sso' => $username,
			'status_domain' => 1,
			'file_kontrak' => $file_name,
			'sign_date' => $signed_date,
			'expired_date' => $expired_date
		);

		$where = array('nama_domain' => $name);
		$insert = $this->m_crud->edit($data, $where, 'domain');

		if ($signed_date != '') {
			$this->update_history($name, $signed_date, $expired_date);
		}

		redirect('domains','refresh');
	}

	public function file_contract()
	{
		$where = 'file_kontrak NOT LIKE ""';
		$data['kontrak'] = $this->m_crud->get_where($where, 'domain')->result();
		$this->load->view('domain/domain_contract', $data);
	}

	public function file_upload()
	{
		if (null !== $this->input->post('id')) {
			$id = $this->input->post('id');
			$file_name = $this->input->post('filename');

			$where = array('nama_domain' => $id);
			$data = array('file_kontrak' => $file_name);

			$update = $this->m_crud->edit($data, $where, 'domain');

			echo json_encode($update);

		} else {
			$config['upload_path']          = './kontrak/';
			$config['allowed_types']        = 'gif|jpg|png|pdf';

			$this->load->library('upload', $config);

			$this->upload->do_upload('file');
		}
	}

	public function update_history($nama_domain, $sign_date, $expired_date)
	{
		$data = array(
			'nama_domain' => $nama_domain,
			'sign_date' => $sign_date,
			'expired_date' => $expired_date
		);

		$insert = $this->m_crud->add($data, 'history_sign');
	}

	public function server()
	{
		$data['server'] = $this->m_crud->get_table('lokasi_server')->result();
		$this->load->view('domain/domain_server', $data);
	}

	public function server_add()
	{
		$jenis = $this->input->post('jenis');
		$ip = $this->input->post('ip');

		$data = array('jenis' => $jenis, 'ip_address' => $ip);
		$insert = $this->m_crud->add($data, "lokasi_server");

		echo json_encode($insert);
	}

	public function server_edit()
	{
		$id = $this->input->post('id');
		$jenis = $this->input->post('jenis');
		$ip = $this->input->post('ip');

		$data = array('jenis' => $jenis, 'ip_address' => $ip);
		$where = array('id_lokasi' => $id);
		$edit = $this->m_crud->edit($data, $where, "lokasi_server");

		echo json_encode($edit);
	}

	public function server_delete()
	{
		$id = $this->input->post('id');

		$where = array('id_lokasi' => $id);
		$delete = $this->m_crud->delete($where, "lokasi_server");

		echo json_encode($delete);
	}

	public function type()
	{
		$data['type'] = $this->m_crud->get_table('domain_type')->result();
		$this->load->view('domain/domain_type', $data);
	}

	public function type_add()
	{
		$type = $this->input->post('type');
		$data = array('type' => $type);
		$insert = $this->m_crud->add($data, "domain_type");

		echo json_encode($insert);
	}

	public function type_edit()
	{
		$id = $this->input->post('id');
		$type = $this->input->post('type');

		$data = array('type' => $type);
		$where = array('id_type' => $id);

		$update = $this->m_crud->edit($data, $where, "domain_type");

		echo json_encode($update);
	}

	public function type_delete()
	{
		$id = $this->input->post('id');

		$where = array('id_type' => $id);

		$delete = $this->m_crud->delete($where, "domain_type");

		echo json_encode($delete);
	}

}

/* End of file domains.php */
/* Location: ./application/controllers/domains.php */