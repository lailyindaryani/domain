<div class="left side-menu">

    <div class="slimscroll-menu" id="remove-scroll">

        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="<?php echo base_url('assets') ?>/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
            </div>
            <h5><a href="#"><?php echo $this->session->userdata('name'); ?></a> </h5>
            <p class="text-muted">Administrator</p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <!--<li class="menu-title">Navigation</li>-->

                <li>
                    <a href="<?php echo base_url('dashboard') ?>">
                        <i class="fi-air-play"></i><span> Home </span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fi-globe"></i> <span> Domains </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <!-- <li><a href="<?php echo base_url('domains') ?>">Dashboard</a></li> -->
                        <li><a href="<?php echo base_url('domains/add') ?>">Add Domain</a></li>
                        <li><a href="<?php echo base_url('domains/nonaktif') ?>">Deleted Domains</a></li>
                        <li><a href="<?php echo base_url('domains/type') ?>">Domain's Categories</a></li>
                        <li><a href="<?php echo base_url('domains/server') ?>">Server Locations</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fi-head"></i><span> PIC </span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo base_url('pic') ?>">List</a></li>
                        <li><a href="<?php echo base_url('pic/add') ?>">Add PIC</a></li>
                        <li><a href="<?php echo base_url('pic/department') ?>">Lokasi Kerja</a></li>
                    </ul>
                </li>

                <li>
                    <a href="<?php echo base_url('admin') ?>"><i class="fa fa-lock"></i> <span> Admin </span></a>
                </li>

                <li>
                    <a href="<?php echo base_url('domains/file_contract') ?>"><i class="fi-cog"></i><span> File Kontrak </span><span class="menu-arrow"></span></a>
                </li>

            </ul>

        </div>
        <!-- Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>