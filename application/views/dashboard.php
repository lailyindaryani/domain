<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('include/sidebar'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <?php $this->load->view('include/topbar'); ?>
                <!-- Top Bar End -->

                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row text-center">
                            <div class="col-sm-6 col-xl-3">
                                <div class="card-box widget-flat border-custom bg-custom text-white">
                                    <i class="fi-tag"></i>
                                    <h3 class="m-b-10"><?php echo $aktif ?></h3>
                                    <p class="text-uppercase m-b-5 font-13 font-600">Domain Active</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="card-box bg-primary widget-flat border-primary text-white">
                                    <i class="fi-archive"></i>
                                    <h3 class="m-b-10"><?php echo $Fakultas ?></h3>
                                    <p class="text-uppercase m-b-5 font-13 font-600">Domain Fakultas</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="card-box widget-flat border-success bg-success text-white">
                                    <i class="fi-help"></i>
                                    <h3 class="m-b-10"><?php echo $Prodi ?></h3>
                                    <p class="text-uppercase m-b-5 font-13 font-600">Domain Prodi</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="card-box bg-danger widget-flat border-danger text-white">
                                    <i class="fi-delete"></i>
                                    <h3 class="m-b-10"><?php echo $Unit ?></h3>
                                    <p class="text-uppercase m-b-5 font-13 font-600">Domain Unit/Direktorat</p>
                                </div>
                            </div>
                        </div>

                        <!-- sign modal content -->
                        <div id="sign-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="signModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Sign Date</h4>
                                    </div>
                                    <form role="form">
                                        <div class="modal-body">
                                            <input type="hidden" name="id_domain" value="" id="sign_id">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Sign Date</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="sign_date" aria-describedby="signDate" placeholder="Enter sign date">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Expired Date</label>
                                                <input type="text" class="form-control" id="expired_date" aria-describedby="expiredDate">
                                                <small id="emailHelp" class="form-text text-muted">We'll calculate when the domain will be disabled.</small>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" id="sign">Save changes</button>
                                        </div>
                                    </form>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <!-- upload file modal content -->
                        <div id="upload-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="uploadModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Upload File Contract</h4>
                                    </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card-box">
                                                    <p class="text-muted font-14 m-b-10">
                                                        Select contract file for this domain
                                                    </p>
                                                    <form action="<?php echo base_url('domains/file_upload') ?>" class="dropzone" id="dropzone" role="form" enctype="multipart/form-data" method="post">
                                                        <input type="hidden" name="id_domain" value="" id="file_id">
                                                        <input type="hidden" name="file_name" value="" id="file_name">
                                                        <div class="fallback">
                                                            <input name="file" type="file" id="file" />
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" id="upload">Save changes</button>
                                        </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <!-- contract file modal content -->
                        <div id="file-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="contractModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">File Contract</h4>
                                    </div>
                                        <div class="row">
                                            <div class="col-12 text-sm-center">
                                                <canvas id="the-canvas"></canvas>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Close</button>
                                        </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <!-- pic modal content -->
                        <div id="pic-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="picModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Set PIC</h4>
                                    </div>
                                    <form role="form">
                                        <div class="modal-body">
                                            <input type="hidden" name="id_domain" value="" id="pic_set_id">
                                            <div class="form-group row col-sm-12">
                                                <select class="form-control select2" id="pic">
                                                    <option value="sel" selected disabled>&nbsp;</option>
                                                    <?php foreach ($pic as $key): ?>
                                                        <option value="<?php echo $key->username_sso ?>"><?php echo $key->username_sso ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>

                                            <dl class="row mb-0">
                                                <dt class="col-sm-3">Username</dt>
                                                <dd class="col-sm-9">
                                                    <p><strong id="username">Username</strong></p>
                                                </dd>
        
                                                <dt class="col-sm-3">Name</dt>
                                                <dd class="col-sm-9">
                                                    <p id="fullname">Full Name</p>
                                                </dd>

                                                <dt class="col-sm-3">Email</dt>
                                                <dd class="col-sm-9">
                                                    <p id="email">Email</p>
                                                </dd>

                                                <dt class="col-sm-3">Phone</dt>
                                                <dd class="col-sm-9">
                                                    <p id="hp">No. HP</p>
                                                </dd>

                                                <dt class="col-sm-3">Department</dt>
                                                <dd class="col-sm-9">
                                                    <p id="jabatan">Jabatan dan Lokasi Kerja</p>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" id="set_pic">Assign PIC</button>
                                        </div>
                                    </form>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <button type="button" class="btn btn-custom btn-rounded w-md waves-effect waves-light float-right" onclick="window.location.href='<?php echo base_url('domains/add') ?>'">New Domain</button>
                                    <h4 class="m-t-0 header-title">List Domains</h4>
                                    <br>

                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-12 text-sm-center form-inline">
                                                <label class="form-inline mr-3">
                                                    Show:
                                                    <select id="demo-show-entries" class="form-control form-control-sm ml-1 mr-1">
                                                        <option value="5">5</option>
                                                        <option value="10">10</option>
                                                        <option value="15">15</option>
                                                        <option value="20">20</option>
                                                    </select>
                                                </label>
                                                <div class="form-group mr-2">
                                                    <select id="demo-foo-filter-status" class="custom-select">
                                                        <option value="">Show all</option>
                                                        <option value="active">Active</option>
                                                        <option value="suspended">Suspended</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <input id="demo-foo-search" type="text" placeholder="Search" class="form-control" autocomplete="on">
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <table id="table-domain" class="table m-b-0 table-bordered toggle-arrow-tiny" data-page-size="5">
                                        <thead>
                                            <tr>
                                                <th data-toggle="true"> Domain </th>
                                                <th> Kategori </th>
                                                <th> Jenis Lokasi </th>
                                                <th> Expired Date </th>
                                                <th> Status </th>
                                                <th> Nama Admin </th>
                                                <th data-hide="all"> e-Mail </th>
                                                <th data-hide="all"> No. HP </th>
                                                <th data-hide="all"> Jabatan </th>
                                                <th data-hide="all"> Lokasi Kerja </th>
                                                <th data-hide="all"> User domain </th>
                                                <th data-hide="all"> IP Address </th>
                                                <th data-hide="all"> Created Date </th>
                                                <th data-hide="all"> Sign Date </th>
                                                <th> Kontrak </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $now = time();
                                            foreach ($domain as $key): 
                                                $your_date = strtotime($key->expired_date);
                                                $datediff = $your_date - $now;
                                                $selisih = round($datediff / (60 * 60 * 24));
                                            ?>
                                            <tr>
                                                <td><?php echo $key->nama_domain ?></td>
                                                <td><?php echo $key->type ?></td>
                                                <td><?php echo $key->jenis ?></td>
                                                <td>
                                                    <?php if ($key->expired_date == '0000-00-00'): ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-info set_pic btn-rounded btn-sm" title="Set sign date for this domain" id="sign_button" data-id="<?php echo $key->nama_domain ?>"> <i class="fa fa-calendar"></i> </button>
                                                    <?php else: ?>
                                                        <?php
                                                        echo date("j F Y", strtotime($key->expired_date));
                                                        echo "<br>";
                                                        if ($selisih < 0) {
                                                            echo "<p class='text-danger'>( " . $selisih . " )</p>";
                                                        }
                                                        ?>          
                                                    <?php endif ?>
                                                </td>
                                                <td class="text-sm-center">
                                                    <?php if ($selisih > -367 and $selisih <= 0 and $key->status_domain != 2): ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-warning btn-rounded btn_suspend" title="Suspend this domain" data-id="<?php echo $key->nama_domain ?>"> <i class="fa fa-history"></i> </button>
                                                    <?php else: ?>
                                                        <?php if ($key->status_domain == 1): ?>
                                                        <span class="badge label-table badge-success">Active
                                                        <?php elseif($key->status_domain == 2): ?>
                                                        <span class="badge label-table badge-warning">Suspended
                                                        <?php endif ?>
                                                    <?php endif ?>
                                                </span></td>
                                                <td>
                                                    <?php if (is_null($key->username_sso)): ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-info btn-sm set_pic btn-rounded" title="Set PIC for this domain" data-toggle="modal" data-target="#pic-modal" data-id="<?php echo $key->nama_domain ?>" id='pic_button'> <i class="fi-head"></i> </button>
                                                    <?php else: ?>
                                                        <?php echo $key->nama_admin ?>
                                                    <?php endif ?>
                                                </td>
                                                <td><?php echo $key->email ?></td>
                                                <td><?php echo $key->no_hp ?></td>
                                                <td><?php echo $key->jabatan ?></td>
                                                <td><?php echo $key->unit ?></td>
                                                <td><?php echo $key->username_sso ?></td>
                                                <td><?php echo $key->ip_address ?></td>
                                                <td><?php echo $key->created_date ?></td>
                                                <td><?php echo $key->sign_date ?></td>
                                                <td>
                                                    <?php if (null === $key->file_kontrak): ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-info btn-sm upload_button" title="Upload contract file for this domain" data-toggle="modal" data-target="#upload-modal" data-id="<?php echo $key->nama_domain ?>"> <i class="fi-upload"></i> </button>
                                                    <?php else: ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-light view_contract" title="View contract for this domain" data-filename="<?php echo $key->file_kontrak ?>" id='file_button'> <i class="fi-paper-clip"></i> </button>
                                                    <?php endif ?>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-icon waves-effect btn-primary btn-rounded" title="Edit this domain" onclick="location.href='<?php echo base_url('domains/edit/') . $key->nama_domain ?>'"> <i class="fa fa-pencil"></i> </button>
                                                    <?php
                                                    if (($selisih > -367 and $selisih <= 0) or $key->status_domain == 2): ?>
                                                        <button type="button" class="btn btn-icon waves-effect btn-warning btn-rounded" title="Reactivate this domain" onclick="location.href='<?php echo base_url('domains/activation/') . $key->nama_domain ?>'"> <i class="fa fa-history"></i> </button>
                                                    <?php endif ?>
                                                    <button type="button" class="btn btn-icon waves-effect btn-danger btn-rounded btn_delete" title="Nonactivate this domain" data-id="<?php echo $key->nama_domain ?>"> <i class="fi-lock"></i> </button>
                                                </td>
                                            </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                        <tfoot>
                                            <tr class="active">
                                                <td colspan="15">
                                                    <div class="text-right">
                                                        <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>                                    

                                </div>

                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php $this->load->view('include/footer'); ?>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <script src="<?php echo base_url('assets') ?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>

        <!--FooTable-->
        <script src="<?php echo base_url('assets') ?>/plugins/footable/js/footable.all.min.js"></script>

        <!-- Tooltipster js -->
        <script src="<?php echo base_url('assets') ?>/plugins/tooltipster/tooltipster.bundle.min.js"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?php echo base_url('assets') ?>/plugins/sweet-alert/sweetalert2.min.js"></script>

        <!-- Select2 -->
        <script src="<?php echo base_url('assets') ?>/plugins/select2/js/select2.min.js" type="text/javascript"></script>

        <!-- Dropzone js -->
        <script src="<?php echo base_url('assets') ?>/plugins/dropzone/dropzone.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>

        <script>
            
        </script>

        <script>
            $(".select2").select2();
            $('#pic').select2('val', 'sel');

            $('#sign_date').datepicker({
                format: 'yyyy-mm-dd'
            });

            $('#sign_button').click(function() {
                <?php if (is_null($key->username_sso)): ?>
                    swal({
                        title: 'Error',
                        text: "You have to set PIC for this domain!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                <?php else: ?>
                    $("#sign_id").val($(this).data("id"))
                    $('#sign_date').datepicker('setDate', 'today'); var sign_date =
                    $('#sign_date').datepicker('getDate');
                    sign_date.setFullYear(sign_date.getUTCFullYear() + 1);
                    sign_date.setDate(sign_date.getDate() + 1);

                    $('#expired_date').val(sign_date.toISOString().slice(0,10));

                    $('#sign-modal').modal('show');
                <?php endif ?>
            });

            $('#sign_date').datepicker().on('change', function() {
                sign = $('#sign_date').datepicker('getDate');;
                sign.setFullYear(sign.getFullYear() + 1);
                sign.setDate(sign.getDate() + 1);

                $('#expired_date').val(sign.toISOString().slice(0,10));
            });

            $('#sign').click(function () {
                id = $('#sign_id').val();
                sign_date = $('#sign_date').val();
                expired_date = $('#expired_date').val();

                $.ajax({
                    url: '<?php echo base_url('domains/set_date') ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {'id': id, 'sign_date': sign_date, 'expired_date': expired_date},
                })
                .done(function() {
                    $('#sign-modal').modal('hide');
                    swal({
                        title: 'Good job!',
                        text: 'Expired date has been assigned!',
                        type: 'success',
                        timer: 3000,
                        confirmButtonClass: 'btn btn-confirm'
                    }).then(function() {
                        location.reload();
                    });
                })
                .fail(function() {
                    swal({
                        title: 'Something error',
                        text: "Please notify the administrator!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                })
                .always(function(res) {
                    console.log(res);
                });
            });

            $('.btn_suspend').click(function() {
                id = $(this).data("id");

                $.ajax({
                    url: '<?php echo base_url('domains/suspend') ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {'id': id},
                })
                .done(function() {
                    $('#sign-modal').modal('hide');
                    swal({
                        title: 'Good job!',
                        text: 'Account has been suspended!',
                        type: 'success',
                        timer: 3000,
                        confirmButtonClass: 'btn btn-confirm'
                    }).then(function() {
                        location.reload();
                    });
                })
                .fail(function() {
                    swal({
                        title: 'Something error',
                        text: "Please notify the administrator!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                })
                .always(function(res) {
                    console.log(res);
                });
            });

            $(".btn_delete").click(function() {
                id = $(this).data("id");
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    $.ajax({
                        url: '<?php echo base_url('domains/delete') ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {'id': id},
                    })
                    .done(function() {
                        swal({
                            title: 'Deleted !',
                            text: "Your record has been deleted",
                            type: 'success',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        }).then(function() {
                            location.reload();
                        });
                    })
                    .fail(function(res) {
                        swal({
                            title: 'Error',
                            text: "Deleting record failed!",
                            type: 'error',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        });
                        console.log(res);
                    });
                    
                });
            });

            $('#pic_button').click(function() {
                $("#pic_set_id").val($(this).data("id"))
            })

            $('#pic').on("select2:select", function(e) { 
                id = $(this).val();

                $.ajax({
                   url: '<?php echo base_url('pic/get_pic_by_id') ?>',
                   type: 'post',
                   dataType: 'json',
                   data: {'id': id},
                })
                .done(function(res) {
                    $("#username").text(res.username_sso);
                    $('#fullname').text(res.nama_admin);
                    $('#email').text(res.email);
                    $('#hp').text(res.no_hp);
                    $('#jabatan').text(res.jabatan + " - " + res.unit)
                })
                .fail(function() {
                    swal({
                        title: 'Error',
                        text: "Something error! Please contact the administrator!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                });
            });

            $('#set_pic').click(function() {
                pic = $('#pic').select2('val');
                id = $('#pic_set_id').val();

                if (pic == 'sel') {
                    swal({
                        title: 'Error',
                        text: "You must select username!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                } else {
                    $.ajax({
                        url: '<?php echo base_url('domains/set_pic') ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {'pic': pic, 'id': id},
                    })
                    .done(function() {
                        $('#pic-modal').modal('hide');
                        swal({
                            title: 'Good job!',
                            text: 'PIC has been assigned!',
                            type: 'success',
                            timer: 3000,
                            confirmButtonClass: 'btn btn-confirm'
                        }).then(function() {
                            $('#pic').select2('val', 'sel');
                            location.reload();
                        });
                    })
                    .fail(function() {
                        swal({
                            title: 'Something error',
                            text: "Please notify the administrator!",
                            type: 'error',
                            confirmButtonClass: 'btn btn-confirm mt-2'
                        });
                    });
                }
            })

            var domain = $('#table-domain');
            domain.footable();

            $('#demo-show-entries').change(function (e) {
                e.preventDefault();
                var pageSize = $(this).val();
                domain.data('page-size', pageSize);
                domain.trigger('footable_filter');
            });

            domain.footable().on('footable_filtering', function (e) {
                var selected = $('#demo-foo-filter-status').find(':selected').val();
                e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                e.clear = !e.filter;
            });

            // Filter status
            $('#demo-foo-filter-status').change(function (e) {
                e.preventDefault();
                domain.trigger('footable_filter', {filter: $(this).val()});
            });

            // Search input
            $('#demo-foo-search').on('input', function (e) {
                e.preventDefault();
                domain.trigger('footable_filter', {filter: $(this).val()});
            });

            $('.view_contract').tooltipster();
            $('.set_pic').tooltipster();

            $('#upload_button').click(function() {
                $("#file_id").val($(this).data("id"))
            })

            file_name = '';

            Dropzone.options.dropzone = {
                maxFiles: 1,
                uploadMultiple: false,
                success: function(file) {
                    $('#file_name').val(file.name);
                }
            };

            $('#upload').click(function() {
                file_name = $('#file_name').val();
                file_id = $('#file_id').val();

                console.log(file_id);
                console.log(file_name);

                $.ajax({
                    url: '<?php echo base_url('domains/file_upload') ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {'id': file_id, 'filename': file_name},
                })
                .done(function() {
                    $('#upload-modal').modal('hide');
                    swal({
                        title: 'Good job!',
                        text: 'File contract has been uploaded!',
                        type: 'success',
                        timer: 3000,
                        confirmButtonClass: 'btn btn-confirm'
                    }).then(function() {
                        location.reload();
                    });
                })
                .fail(function() {
                    swal({
                        title: 'Something error',
                        text: "Please notify the administrator!",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                });
            })

            $('#file_button').click(function () {
                file_name = $(this).data("filename");
                var url = 'http://localhost/domain/kontrak/' + file_name;

                // Loaded via <script> tag, create shortcut to access PDF.js exports.
                var pdfjsLib = window['pdfjs-dist/build/pdf'];

                // The workerSrc property shall be specified.
                pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

                // Asynchronous download of PDF
                var loadingTask = pdfjsLib.getDocument(url);
                loadingTask.promise.then(function(pdf) {
                    console.log('PDF loaded');
                  
                    // Fetch the first page
                    var pageNumber = 1;
                    pdf.getPage(pageNumber).then(function(page) {
                        console.log('Page loaded');
                        
                        var scale = 1.5;
                        var viewport = page.getViewport({scale: scale});

                        // Prepare canvas using PDF page dimensions
                        var canvas = document.getElementById('the-canvas');
                        var context = canvas.getContext('2d');
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;

                        // Render PDF page into canvas context
                        var renderContext = {
                          canvasContext: context,
                          viewport: viewport
                        };
                        var renderTask = page.render(renderContext);
                        renderTask.promise.then(function () {
                          console.log('Page rendered');
                        });
                    });
                }, function (reason) {
                    // PDF loading error
                    console.error(reason);
                });
                $('#file-modal').modal('show');
            })
        </script>

    </body>
</html>