<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class maintenance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('maintenance');
	}

	public function err()
	{
		$this->load->view('upload_success');
	}

}

/* End of file maintenance.php */
/* Location: ./application/controllers/maintenance.php */