<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_join');
		$this->load->model('m_crud');
		if ($this->session->has_userdata('username') == false) {
			redirect('auth');
		}
	}

	public function index()
	{
		$jumlah = $this->m_join->domain_type_group()->result();
		
		$data['domain'] = $this->m_join->domain_pic_unit_active()->result();
		$data['pic'] = $this->m_join->pic_unit()->result();
		$data['aktif'] = $this->m_join->domain_pic_unit_active()->num_rows();
		$data['Fakultas'] = 0;
		$data['Prodi'] = 0;
		$data['Unit'] = 0;

		foreach ($jumlah as $value) {
			$data[$value->type] = $value->jumlah;
		}

		$this->load->view('dashboard', $data);
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */