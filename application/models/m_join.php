<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_join extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function pic_unit()
	{
		$this->db->select('username_sso, nama_admin, email, no_hp, jabatan, unit');
		$this->db->from('pic');
		$this->db->join('lokasi_kerja', 'pic.lokasi_kerja = lokasi_kerja.id_lokasi', 'left');
		return $this->db->get();
	}

	public function pic_unit_by_id($id)
	{
		$this->db->select('username_sso, nama_admin, email, no_hp, jabatan, unit');
		$this->db->from('pic');
		$this->db->where('username_sso =', $id);
		$this->db->join('lokasi_kerja', 'pic.lokasi_kerja = lokasi_kerja.id_lokasi', 'left');
		$this->db->limit(1);
		return $this->db->get();
	}

	public function domain_pic_unit_active()
	{
		$this->db->select('nama_domain, domain.username_sso as username_sso, nama_admin, email, type, status_domain, jenis, created_date, sign_date, expired_date, user_domain, ip_address, file_kontrak, no_hp, jabatan, unit');
		$this->db->from('domain');
		$this->db->where('status_domain not like', 3);
		$this->db->join('pic', 'domain.username_sso = pic.username_sso', 'left');
		$this->db->join('lokasi_kerja', 'pic.lokasi_kerja = lokasi_kerja.id_lokasi', 'left');
		$this->db->join('lokasi_server', 'lokasi_server.id_lokasi = domain.id_lokasi', 'left');
		$this->db->join('domain_type', 'domain_type.id_type = domain.id_type', 'left');
		$this->db->order_by('created_date', 'desc');
		return $this->db->get();
	}

	public function domain_pic_unit_nonaktif()
	{
		$this->db->select('nama_domain, domain.username_sso as username_sso, nama_admin, email, type, status_domain, jenis, created_date, sign_date, expired_date, user_domain, ip_address, file_kontrak, no_hp, jabatan, unit');
		$this->db->from('domain');
		$this->db->where('status_domain like', 3);
		$this->db->join('pic', 'domain.username_sso = pic.username_sso', 'left');
		$this->db->join('lokasi_kerja', 'pic.lokasi_kerja = lokasi_kerja.id_lokasi', 'left');
		$this->db->join('lokasi_server', 'lokasi_server.id_lokasi = domain.id_lokasi', 'left');
		$this->db->join('domain_type', 'domain_type.id_type = domain.id_type', 'left');
		return $this->db->get();
	}

	public function domain_type_group()
	{
		$this->db->select('type, COUNT(nama_domain) as jumlah');
		$this->db->from('domain_type');
		$this->db->join('domain', 'domain_type.id_type = domain.id_type', 'left');
		$this->db->where('status_domain', 1);
		$this->db->group_by('type');
		return $this->db->get();
	}

	public function domain_full($id)
	{
		$this->db->select('nama_domain, username_sso, type, status_domain, jenis, created_date, sign_date, expired_date, user_domain, ip_address, file_kontrak');
		$this->db->from('domain');
		$this->db->join('lokasi_server', 'lokasi_server.id_lokasi = domain.id_lokasi', 'left');
		$this->db->join('domain_type', 'domain_type.id_type = domain.id_type', 'left');
		$this->db->where('nama_domain', $id);
		return $this->db->get();
	}

}

/* End of file m_join.php */
/* Location: ./application/models/m_join.php */