<!doctype html>
<html lang="en">

    <?php $this->load->view('include/header'); ?>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('include/sidebar'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <?php $this->load->view('include/topbar'); ?>
                <!-- Top Bar End -->

                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <!-- upload file modal content -->
                        <div id="upload-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="uploadModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Upload File Contract</h4>
                                    </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card-box">
                                                    <p class="text-muted font-14 m-b-10">
                                                        Maximal size file : 5 Mb
                                                    </p>
                                                    <form action="#" class="dropzone" id="dropzone" role="form" enctype="multipart/form-data">
                                                        <div class="fallback">
                                                            <input name="userfile" type="file" multiple />
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" id="upload">Save changes</button>
                                        </div>
                                                    </form>


                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <button type="button" class="btn btn-custom btn-rounded w-md waves-effect waves-light float-right" data-toggle="modal" data-target="#upload-modal" id='upload_button'><i class="mdi mdi-upload"></i> Upload Files</button>
                                    <h4 class="header-title m-b-30">Contract Files</h4>

                                    <div class="row">
                                        <?php foreach ($kontrak as $key): ?>
                                        <div class="col-lg-3 col-xl-2">
                                            <div class="file-man-box">
                                                <!-- <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a> -->
                                                <div class="file-img-box">
                                                    <img src="<?php echo base_url('assets') ?>/images/file_icons/pdf.svg" alt="icon">
                                                </div>
                                                <a href="<?php echo base_url('kontrak/') ?>kartu.pdf" class="file-download"><i class="mdi mdi-download"></i> </a>
                                                <div class="file-man-title">
                                                    <h5 class="mb-0 text-overflow"><?php echo $key->file_kontrak ?></h5>
                                                    <p class="mb-0"><small><?php echo $key->nama_domain ?></small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach ?>
                                    </div>

                                   <!--  <div class="text-center mt-3">
                                        <button type="button" class="btn btn-outline-danger w-md waves-effect waves-light"><i class="mdi mdi-refresh"></i> Load More Files</button>
                                    </div>
 -->
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php $this->load->view('include/footer'); ?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/waves.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>

        <!-- Dropzone js -->
        <script src="<?php echo base_url('assets') ?>/plugins/dropzone/dropzone.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.core.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/jquery.app.js"></script>

        <script>
            $('#dropzone').submit(function(e){
                e.preventDefault();
                data = new FormData(this);
                console.log(data);
                // $.ajax({
                //     url: '<?php echo base_url('domains/file_upload') ?>',
                //     type: 'post',
                //     // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                //     // data: {param1: 'value1'},
                // })
                // .done(function() {
                //     console.log("success");
                // })
                // .fail(function() {
                //     console.log("error");
                // })
                // .always(function() {
                //     console.log("complete");
                // });
                
                // $.ajax({
                //     url:'<?php echo base_url('domains/file_upload');?>',
                //     type:"post",
                //     data:new FormData(this),
                //     processData:false,
                //     contentType:false,
                //     cache:false,
                //     async:false,
                //     success: function(data){
                //         console.log(data);
                //         // alert("Upload Image Successful.");
                //     }
                // });
            });
        </script>

    </body>
</html>